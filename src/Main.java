import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class Main {
    public static void main(String[] args) {

        escribirArchivo();
        leerArchivo();


    }

    public static void leerArchivo(){
        try {
            String contenido = Files.readString(Path.of("MiData.txt"));
            System.out.println("Contenido del archivo: " + contenido);
        } catch (IOException e) {
            System.out.println("Error al leer archivo: " + e.getMessage());
        }
    }

    public static void escribirArchivo(){
        File archivo = new File("MiData.txt");
        try {
            FileWriter escritor = new FileWriter(archivo);
            escritor.write("Informacion para el archivo");
            escritor.close();
            System.out.println("Archivo creado exitosamente.");
        } catch (IOException e) {
            System.out.println("Error al crear archivo: " + e.getMessage());
        }
    }
}